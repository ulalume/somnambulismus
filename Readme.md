# Somnambulismus

**Virtuální oddělení astroinformatiky**

si Vás dovoluje pozvat na praktický miniseminář

## Lectures on scientific computing with Python

by

*Robert Johansson*

<https://github.com/jrjohansson/scientific-python-lectures>

který se uskuteční ve dnech **5.–7. dubna 2013** v prostorách hvězdárny **Vyškov**.

Seminář je organizován netradičně během Měsíčního úplňku v rámci již tradičního Vyškovského novoluní.

### Zaměření

Cílem semináře je seznámení účastníků od základních až k pokročilejším metodám využití výpočetní techniky ke zefektivnění vědecké práce. Hlavní důraz bude kladen na analýzu dat a prezentaci jejich výsledků. Pracovním jazykem kromě češtiny a slovenštiny bude pochopitelně python.

### Požadavky

Od potencionálních účastníků se očekává vážný zájem o programování. Předchozí zkušenosti nejsou nutné, nicméně vítané. Ne nezbytnou, avšak výhodnou pomůckou bude vlastní notebook s nainstalovaným interpretem jazyka python ve verzi alespoň 2.7.x. Dále pak moderní webový prohlížeč a fungující prostředí IPython notebook a dodatečné balíky NumPy, SciPy, Matplotlib, SymPy a PyFITS.

### Ubuntu

V aktuální verzi operačního systému Ubuntu nainstalujete všechny potřebné balíčky zadáním příkazů:

    $ sudo apt-get install python ipython ipython-notebook
    $ sudo apt-get install python-numpy python-scipy python-matplotlib
    $ sudo apt-get install python-sympy
    $ sudo apt-get install python-pyfits

### Program

**Pátek**

* Lecture-0 Scientific Computing with Python (*Zdeněk Janák*)
* Lecture-1 Introduction to Python Programming (*Jiří Květák*)

**Sobota**

* Lecture-2 Numpy - multidimensional data arrays (*Matuš Kočka*)
* Lecture-3 Scipy - Library of scientific algorithms (*Matuš Kočka*)
* Lecture-4 Matplotlib - 2D and 3D plotting (*Michal Prišegen*)
* Lecture-5 Sympy - Symbolic algebra (*Petr Zikán*)

**Neděle**

* Lecture-6 PyFITS - Python library providing access to FITS files (*Filip Hroch*)
* Lecture-7 Revision Control Software (*Jaroslav Vážný*)
* FreeFITS - Úprava licenčních informací v hlavičce FITS snímku (*Zdeněk Janák*)

### Výstup

Po absolvování semináře by účastníci měli být schopni samostatně i týmově vytvořit jednoduchý program pro manipulaci s FITS snímky. Jako dobrovolně povinné domácí cvičení bude zadáno vytvoření skriptu pro kompletaci hlaviček FITS snímků pořízených na Hvězdárně ve Vyškově.

### Organizace

Seminář se bude konat v prostorách [hvězdárny Vyškov](http://www.zoo-vyskov.cz/hvezdarna/index.html) a seminární místnosti ZOO Parku ve [Vyškově](http://www.vyskov-mesto.cz/). Noclech je pro přihlášené účastníky zajištěn na hvězdárně. Jelikož její kapacity jsou omezeny, je omezen i maximální počet účástníků na ±15. Hvězdárna je vybavena důstojným sociálním zařízením a skromnou kuchyňkou. Lůžková kapacita je dostatečná pro zhruba polovinu účastníků, zbylí se budou muset spokojit s podlahou a vlastním spacím pytlem. Výběr lůžek pro přihlášené účastníky bude proveden losem na bázi generátoru náhodných čísel modulu [random](http://docs.python.org/3.3/library/random.html) v pythonu. Losování paland proběhne úderem ENTERu ve středu 3. dubna 2013 v levé krněnské polodne. Doprava a strava je zcela v režii účastníků. Přezuvky s sebou!

![Mapa](raw/master/mapa.png)
[Zvětšit mapu](http://maps.google.cz/maps?q=49.283785+17.0224070&ie=UTF8&t=m&brcurrent=5,0,0&ll=49.28382,17.022457&spn=0.04703,0.102825&z=13&source=embed)

### Přihláška

Zájemci se hlaste e-m@ilem na adresu <janak@physics.muni.cz> do **31. března 2013**. Na shledanou se za organizátory těší Filip Hroch, Zdeněk Janák, Matuš Kočka, Jiří Květoň, Michal Prišegen, Jaroslav Vážný a Petr Zikán.

### Účastníci

* Ondrej Kamenský
* Vladimír Domček
* Zdeněk Prudil
* Mirek Dočekal
* Jakub Vonšovský
* Dávid Nahalka
* Jakub Vulgan
* Honza Okleštěk
* Jakub Bělín
* Šárka Dvořáková
* Matej Špeťko
* Tereza Jeřábková
* Klára Loukotová
* Michal Almáši
* Elena Lindišová

### Sponzoři

![Python](raw/master/python.png)

![IPython](raw/master/ipython.png)

![GitHub](raw/master/github.png)

![Creative Commons](raw/master/cc.png)

### Playlist

BSD (1998), BSP (1992), Priessnitz (1989), Monty Python (1969).
