from pylab import *
from mpl_toolkits.mplot3d.axes3d import Axes3D

t = linspace(0, 30, 1000)
x, y, z = [t*cos(t), t*sin(t), t]
fig = figure()
ax = fig.gca(projection='3d')
ax.plot(x, y, z)
xlabel('x')
ylabel('y')
show()

