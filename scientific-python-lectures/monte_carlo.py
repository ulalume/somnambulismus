#!/bin/python
from random import random
from math import pow, sqrt

DARTS=10000000  # number of steps
hits = 0
throws = 0
for i in range (1, DARTS):
        throws += 1
        x = random()
        y = random()
#       dist = sqrt(pow(x, 2) + pow(y, 2))
        dist = sqrt((x*x)+(y*y))
        if dist <= 1.0:
                hits = hits + 1.0

# hits / throws = 1/4 Pi
pi = 4 * (hits / throws)

print "pi = %s" %(pi)


